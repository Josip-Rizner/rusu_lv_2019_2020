import numpy as np
from sklearn import neural_network
from sklearn import linear_model

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy

def non_func(n):
        x = np.linspace(1,10,n)
        y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
        y_measured = add_noise(y)
        data = np.concatenate((x,y,y_measured),axis = 0)
        data = data.reshape(3,n)
        return data.T
    
np.random.seed(270)
data_train= non_func(500)

np.random.seed(12)
data_test= non_func(100)

#neuronska mreža za regresijski problem
nn=neural_network.MLPRegressor(hidden_layer_sizes=(20,10),verbose=True, max_iter=1700,alpha =0.001) #alpha = parametar regularizacije

#2 sloja, 1. sloj 20 neurona, 2. sloj 10 neurona
nn.fit(data_train[:,[0,2]],data_train[:,1])

y_test_predict_nn= nn.predict(data_test[:,[0,2]])

linear_regresion=linear_model.LinearRegression()
linear_regresion.fit(data_train[:,[0,2]],data_train[:,1])

print('Ispitivanje tocnosti : {:.4f}'.format(nn.score(data_test[:,[0,2]], data_test[:,1])))
print('Ispitivanje tocnosti : {:.4f}'.format(linear_regresion.score(data_test[:,[0,2]], data_test[:,1])))