# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 12:12:07 2021

@author: student
"""


#../resources/mbox.txt
#../resources/mbox-short.txt

confidences = []

fname = input('Enter the file name: ')

try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

for line in fhand:
    if(line.startswith("X-DSPAM-Confidence:")):
        lineParts = line.split()
        confidences.append(float(lineParts[1]))

print("Ime datoteke: ", fname)
print("Average X-DSPAM-Confidence: ", sum(confidences)/len(confidences))