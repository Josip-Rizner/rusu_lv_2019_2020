# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 11:58:42 2021

@author: student
"""

try:  
    broj = float(input("Unesite broj između 0.0 i 1.0: "))

    if broj >= 0 and broj <= 1:
        if(broj >= 0.9):
            print("A")
        elif(broj < 0.9 and broj >= 0.8):
            print("B")
        elif(broj < 0.8 and broj >= 0.7):
            print("C")
        elif(broj < 0.7 and broj >= 0.6):
            print("D")
        else:
            print("F")
    else:
        print("Niste unijeli broj između 0.0 i 1.0")
 

except:
    print("Niste unijeli broj")