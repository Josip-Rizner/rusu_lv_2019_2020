# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 12:33:24 2021

@author: student
"""

#../resources/mbox-short.txt

emails = []
hostnames = dict()
 
fname = input('Enter the file name: ')

try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

for line in fhand:
    if line.startswith("From"):
        lineParts = line.split()
        emails.append(lineParts[1])
        
for email in emails:
    emailParts = email.split("@")
    if emailParts[1] not in hostnames:
        hostnames[emailParts[1]] = 1
    else: 
        hostnames[emailParts[1]] = hostnames[emailParts[1]] + 1
    

    
print(hostnames)