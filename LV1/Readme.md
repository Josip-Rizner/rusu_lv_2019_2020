Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

-raw_input() je promijenjen input()
-u open(fnamex) promijenjen u open(fname)
-dodane zagrade na print


Na današnjim laboratorijskim upoznavali smo se s osnovnim naredbama git-a i s osnovnom sintaksom programskog jezika Python.

-U prvom zadatku smo pomoću input() metode unosili podatke te ih ispisivali pomoću print() metode

- U drugom zadatku smo unosili broj te ispisivali na izlaz odgovarajuću poruku u ovisnosti o intervalu u kojem se taj broj nalazi. U slučaju da je došlo do pogreške koristili smo try i except naredbe.

- U trečem zadatku smo dodali funkciju koja izračunava total_kn.

- U četvrtom zadatku koristimo while petlju iz koje ne izlazimo dok korisnik ne upiše "Done". Svaki broj koji korisnik upiše dodajemo u polje od kojega kasnije tražimo prosjek, duljinu, minimalnu i maksimalnu vrijednost. Od krivog unosa, tj. unosa slova smo se zaštitili try i except naredbama

-U petom zadatku otvaramo tekstualnu datoteku te pregledavamo sadržaj liniju po liniji. Kada naiđemo na liniju koja počinje s određenim stringom, splitamo ju (pomoću split() metode) po razmacima i izvučemo broj koji stavljamo u polje od kojega kasnije računamo prosjek.

-Isto tako, u šestom zadatku otvaramo tekstualnu te pregledavamo sadržaj liniju po liniju. Kada počinje s "From" splitamo ju po razmacima kako izvukli samo e-mail adresu. E-mail adresu splitamo prema '@' znaku te hostname dodajemo u dictionary i brojima koliko puta se taj hostname pojavio u toj datoteci.