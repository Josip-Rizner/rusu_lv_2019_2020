import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 


mtcars = pd.read_csv('../resources/mtcars.csv') 

#1.)
cyl_4 = mtcars[mtcars.cyl == 4].mean()
cyl_mpg_4 = cyl_4['mpg'].mean()

cyl_6 = mtcars[mtcars.cyl == 6]
cyl_mpg_6 = cyl_6['mpg'].mean()

cyl_8 = mtcars[mtcars.cyl == 8]
cyl_mpg_8 = cyl_8['mpg'].mean()

mpgDataCyl = {"cyl": [4, 6, 8], "mpg": [cyl_mpg_4, cyl_mpg_6, cyl_mpg_8]}
mpgCyl = pd.DataFrame(mpgDataCyl, columns = ["cyl", "mpg"])
mpgCyl.plot.bar(x="cyl", y="mpg")


#2)
wtData = {"4": cyl_4["wt"], "6": cyl_6["wt"], "8": cyl_8["wt"]}
wtCyl = pd.DataFrame(wtData, columns = ["4", "6", "8"])
wtCyl.plot.box()


#3)
a_cars = mtcars[mtcars.am == 0]
m_cars = mtcars[mtcars.am == 1]

a_cars_mpg = a_cars['mpg'].mean()
m_cars_mpg = m_cars['mpg'].mean()

amDataMpg = {'am' : ['automatic','manual'], 'mpg':[a_cars_mpg, m_cars_mpg]}
am_mpg = pd.DataFrame(amDataMpg)
am_mpg.plot.bar(x='am', y='mpg')


#4)
colors = np.where(mtcars["am"]==1,'r','b')
mtcars.plot.scatter(x="hp", y="qsec", c = colors)
plt.legend(["Automatic"])
