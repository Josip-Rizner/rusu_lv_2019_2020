import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 


mtcars = pd.read_csv('../resources/mtcars.csv') 

print(mtcars)

#1.)
mtcars_sorted = mtcars.sort_values('mpg')
#print(sorted_mtcars)
print(mtcars_sorted[['car', 'mpg']].tail(5))


#2.)
mtcars_8cyl = mtcars.query('cyl == 8')
print(mtcars_8cyl[['car','mpg','cyl']].sort_values('mpg').head(3))

#3.)
mtcars_6cyl = mtcars.query('cyl == 6')
print(mtcars_6cyl['mpg'].mean())


#4.)
mtcars_4cyl = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2) & (mtcars.wt <= 2.2)]
print(mtcars_4cyl)


#5.)
mtcars_am = mtcars.groupby('am')
print(mtcars_am['am'].count())


#6.)
mtcars_am = mtcars[(mtcars.am == 0) & (mtcars.hp > 100)]
print(mtcars_am)
print(len(mtcars_am))


#7.)
mtcars_in_kg = mtcars.copy();
mtcars_in_kg['wt'] = mtcars_in_kg.wt = mtcars_in_kg.wt * 1000 *0.45359237
print(mtcars_in_kg[['car', 'wt']])  
