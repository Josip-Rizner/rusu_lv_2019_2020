### mtcars.csv

Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models)
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset



### AirQualityRH.py

Skripta za dohvaćanje podataka o kvaliteti zraka pomocu REST API


U ovoj vježbi smo se upoznavali s pandas bibliotekom i grafičkom bibliotekom, tj. grafičkim prikazom podataka.


1. U prvom zadatku smo analizirali podatke učitane iz mtcars.csv datoteke pomoću pandas biblioteke.

2. U drugom zadatku smo prvo analizirali podatke iz mtcars.csv te ih potom grafički prikazivali pomoću pandas biblioteke.

3. u Trećem zadatku smo dohvatili podatke o kvaliteti zraka u json obliku te smo ih analizirali i grafički prikazivali.