import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
 
    
#Generiranje podataka
x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

# make polynomial features
poly1 = PolynomialFeatures(degree=2)
xnew1 = poly1.fit_transform(x)

poly2 = PolynomialFeatures(degree=6)
xnew2 = poly2.fit_transform(x)

poly3 = PolynomialFeatures(degree=15)
xnew3 = poly3.fit_transform(x)
    
np.random.seed(12)
indeksi1 = np.random.permutation(len(xnew1))
indeksi_train1 = indeksi1[0:int(np.floor(0.7*len(xnew1)))]
indeksi_test1 = indeksi1[int(np.floor(0.7*len(xnew1)))+1:len(xnew1)]

np.random.seed(12)
indeksi2 = np.random.permutation(len(xnew2))
indeksi_train2 = indeksi2[0:int(np.floor(0.7*len(xnew2)))]
indeksi_test2 = indeksi2[int(np.floor(0.7*len(xnew2)))+1:len(xnew2)]

np.random.seed(12)
indeksi3 = np.random.permutation(len(xnew3))
indeksi_train3 = indeksi3[0:int(np.floor(0.7*len(xnew3)))]
indeksi_test3 = indeksi3[int(np.floor(0.7*len(xnew3)))+1:len(xnew3)]



#kreiranje varijabli koje sadrže podatke za treniranje i testiranje modela
xtrain1 = xnew1[indeksi_train1,]
xtrain2 = xnew2[indeksi_train2,]
xtrain3 = xnew3[indeksi_train3,]
ytrain1 = y_measured[indeksi_train1]
ytrain2 = y_measured[indeksi_train2]
ytrain3 = y_measured[indeksi_train3]

xtest1 = xnew1[indeksi_test1,]
xtest2 = xnew2[indeksi_test2,]
xtest3 = xnew3[indeksi_test3,]
ytest1 = y_measured[indeksi_test1]
ytest2 = y_measured[indeksi_test2]
ytest3 = y_measured[indeksi_test3]





#treniranje modela
linearModel1 = lm.LinearRegression()
linearModel1.fit(xtrain1,ytrain1)

linearModel2 = lm.LinearRegression()
linearModel2.fit(xtrain2,ytrain2)

linearModel3 = lm.LinearRegression()
linearModel3.fit(xtrain3,ytrain3)

ytest_p1 = linearModel1.predict(xtest1)
ytest_p2 = linearModel2.predict(xtest2)
ytest_p3 = linearModel3.predict(xtest3)

ytrain_p1 = linearModel1.predict(xtrain1)
ytrain_p2 = linearModel2.predict(xtrain2)
ytrain_p3 = linearModel3.predict(xtrain3)

MSE_test = np.array([mean_squared_error(ytest1, ytest_p1),
                     mean_squared_error(ytest2, ytest_p2),
                     mean_squared_error(ytest3, ytest_p3)])
                    
MSE_train = np.array([mean_squared_error(ytrain1, ytrain_p1),
                     mean_squared_error(ytrain2, ytrain_p2),
                     mean_squared_error(ytrain3, ytrain_p3)])                    


print("MSE_test: " + str(MSE_test))
print("MSE_train: " + str(MSE_train))

#pozadinska funkcija vs model
plt.figure(1)
plt.plot(x,y_true,label='pozadinska')
plt.plot(x, linearModel1.predict(xnew1),'r-',label='model1 - degree=2')
plt.plot(x, linearModel2.predict(xnew2),'k-',label='model2 - degree=6')
plt.plot(x, linearModel3.predict(xnew3),'g-',label='model3 - degree=15')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)


#Zadatak 4- model je bolje prilagođen pozadinskoj funkciji, tj. model je bolje naučio pozadinsku funkciju nego u prvom zadatku.


#Zadatak 5 - što je veći degree to je modelj bolje prilagođen trening podacima, tj. krivulja bolje prati trening podatke 