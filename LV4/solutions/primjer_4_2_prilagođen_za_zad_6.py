import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures

def non_func(x):
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    return y

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
 
x = np.linspace(1,10,50)
y_true = non_func(x)
y_measured = add_noise(y_true)

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

# make polynomial features
poly = PolynomialFeatures(degree=15)
xnew = poly.fit_transform(x)
    
poly2 = PolynomialFeatures(degree=15)
xnew2 = poly2.fit_transform(x)

np.random.seed(12)
indeksi = np.random.permutation(len(xnew))
indeksi_train = indeksi[0:int(np.floor(0.7*len(xnew)))]
indeksi_test = indeksi[int(np.floor(0.7*len(xnew)))+1:len(xnew)]


np.random.seed(12)
indeksi2 = np.random.permutation(len(xnew2))
indeksi_train2 = indeksi2[0:int(np.floor(0.7*len(xnew2)))]
indeksi_test2 = indeksi2[int(np.floor(0.7*len(xnew2)))+1:len(xnew2)]

#kreiranje varijabli koje sadrže podatke za treniranje i testiranje modela (70% podataka za treniranje, 30% za testiranje)
xtrain = xnew[indeksi_train,]
ytrain = y_measured[indeksi_train]
xtrain2 = xnew2[indeksi_train2,]
ytrain2 = y_measured[indeksi_train2]

xtest = xnew[indeksi_test,]
ytest = y_measured[indeksi_test]
xtest2 = xnew2[indeksi_test2,]
ytest2 = y_measured[indeksi_test2]


#treniranje modela
linearModel = lm.Ridge(alpha=1000)
linearModel.fit(xtrain,ytrain)


linearModel2 = lm.LinearRegression()
linearModel2.fit(xtrain2,ytrain2)

ytest_p = linearModel.predict(xtest)
ytest_p2 = linearModel2.predict(xtest2)


plt.figure(1)
plt.plot(xtest[:,1],ytest_p,'og',label='predicted')
plt.plot(xtest[:,1],ytest,'or',label='test')
plt.legend(loc = 4)

#pozadinska funkcija vs model
plt.figure(2)
plt.plot(x,y_true,label='pozadinska f-ja')
plt.plot(x, linearModel.predict(xnew),'r-',label='model')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(xtrain[:,1],ytrain,'ok',label='train')
plt.legend(loc = 4)


print('y_hat6 = ', linearModel.intercept_, '+', linearModel.coef_, '*x')
print('y_hat5 = ', linearModel2.intercept_, '+', linearModel2.coef_, '*x')
#S većom vrijednosti dolazi do overfittinga
