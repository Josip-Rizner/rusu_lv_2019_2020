import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans


# funkcija koja dodijeljuje svakom podatku najblizi centar
def assignDataToCenter(X, c):
    
    n,m = np.shape(X)
    K, _ = np.shape(c)
    distance = -1*np.ones((n,K))

    # pronaci udaljenost svakog podatka do svakog centra i dodijeli podatak centru kojem je najblizi
    for j in range(0,K):
        distance[:,j] = np.linalg.norm(X-c[j,:], axis = 1)

    b = np.argmin(distance, axis = 1)
    J = np.min(distance, axis = 1)
    J = J.sum()
    
    return b, J


# funkcija koja izracunava centroid podataka ovisno o pripadnosti
def centroid(X,b,K):
    
    n,m = np.shape(X)    
    centers = np.zeros((K,m))

    for j in range(0,K):
        centers[j,:] = np.sum(X[b==j,:], axis = 0)
        centers[j,:] /= np.sum( 1.0*(b==j))
    
    return centers
    

# generiranje umjetnih podataka
n_samples = 200
X, _ = make_blobs(n_samples=n_samples, centers=3, n_features=2,
                cluster_std=1.5, random_state=144)

# prikazi podatke
plt.figure(1)
plt.title("Podaci")
plt.scatter(X[:,0], X[:,1])
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.show()

# odaberi broj centara i pocetne vrijednosti centara
n, m = np.shape(X)
K = 3

# uzmi nasumicno K podataka i korisi ih kao pocetne centre
centers = X[np.random.choice(X.shape[0], K, replace=False),:]

no_iter = 20
J = np.zeros((no_iter,1))

# iterativni postupak podesavanja centara
for i in range(0,no_iter):
    
    # odredi pripadnost za svaki podatak i vrijednost kriterijske funkcije
    b, J[i,0] = assignDataToCenter(X,centers)
    
    # prikazi centre i podatke ovisno o pripadnosti
    plt.clf()
    plt.scatter(X[:,0], X[:,1], c=b)
    plt.scatter(centers[:,0],centers[:,1], color = 'red', marker = 'x')
    plt.title("Iteracija " +str(i))

    #izracunaj centroid
    centers = centroid(X, b, K)

    plt.pause(1.0)


plt.show()

# prikazi vrijednost kriterijske funkcije kroz iteracije
plt.figure(2)
plt.plot(range(0,no_iter), J)
plt.xlabel("iteracije")
plt.ylabel("J")
plt.title("Kriterijska funkcija")
plt.show()


# Sckit learn kmeans
model = KMeans(n_clusters = K)
model.fit(X)

print("-------------------------")
print("Scikit learn centri:")
print(model.cluster_centers_)
print("-------------------------")
print("Vlastita implementacija kmeans:")
print(centers)


#Iznad je kod s predavanja