from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as cluster


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X





X = generate_data(500, 5)
num_of_clusters = 3;

kMeans = cluster.KMeans(n_clusters = num_of_clusters)

predict = kMeans.fit_predict(X)


plt.scatter(X[:,0], X[:,1], c=predict)
plt.scatter(kMeans.cluster_centers_[:,0], kMeans.cluster_centers_[:,1], color = 'blue') 
plt.show()

#Kada se kod pokreće nekoliko puta, mijenjaju se boje klastera,zato što algoritam svaki puta nasumično bira točku koju isprobava kao centroid.
#Uz način gdje se generiraju kružnice i mjeseci(4,5), algoritam s 3 klastera ne pronalazi dobre centre. 