from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as cluster


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X





X = generate_data(500, 1)
num_of_clusters = 3;

kMeans = cluster.KMeans(n_clusters = num_of_clusters)

predict = kMeans.fit_predict(X)


plt.scatter(X[:,0], X[:,1], c=predict)
plt.scatter(kMeans.cluster_centers_[:,0], kMeans.cluster_centers_[:,1], color = 'blue') 
plt.show()



listRange = list(range(2,20))
critFunction = []
for N_clusters in listRange:
    clusterK = cluster.KMeans(n_clusters = N_clusters)
    predicted = clusterK.fit_predict(X)
    critFunction.append(clusterK.inertia_)


plt.plot(listRange,critFunction)
plt.xlabel("Broj klastera")
plt.ylabel("Kriterijska funkcija")
plt.xticks(listRange)
plt.show()

#Može se vidjeti kako kriterijska funkcija na početku naglo pada, a kasnije usporava
#Optimalni broj klastera se može očitati sa grafa u trenutku kada kriterijska funkcija počinje usporavati. 
