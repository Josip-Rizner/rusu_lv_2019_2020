import scipy as sp 
from sklearn import cluster, datasets 
import numpy as np 
import matplotlib.pyplot as plt 
from sklearn.utils import shuffle
from sklearn.cluster import KMeans

try: 
 face = sp.face(gray=False) 
except AttributeError: 
 from scipy import misc 
 face = misc.face(gray=False) 
 
colors = 8 
X = face.reshape((-1, 3)) # We need an (n_sample, n_feature) array 


X = np.reshape(face, (-1, 3))
X = X / 255
 
x_train = shuffle(X, random_state=0)[:10000]
kmeans = KMeans(n_clusters=colors, random_state=0).fit(x_train)
 
labels = kmeans.predict(X)
centers = kmeans.cluster_centers_
img = np.reshape(centers[labels], face.shape)

plt.figure(1)
plt.subplot(1,2,1)
plt.imshow(face)
plt.title("Originalna")
plt.subplot(1,2,2)
plt.title("Kvantizirana")
plt.imshow(img)