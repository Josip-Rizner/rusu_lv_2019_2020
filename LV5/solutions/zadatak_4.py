import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

image = mpimg.imread('../resources/example_grayscale.png') 

    
X = image.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=3,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
image_compressed = np.choose(labels, values)
image_compressed.shape = image.shape

plt.figure(1)
plt.subplot(1,2,1)
plt.imshow(image,  cmap='gray')
plt.title("Original")
plt.subplot(1,2,2)
plt.title("Compressed")
plt.imshow(image_compressed,  cmap='gray')


#S više klastera dobijemo bolju kvalitetu slike. To je slučaj zato što s više klastera imamo više nijansi sive kako bi prikazali sliku
#S 10 klastera sliku zauzima 25,6 puta manje memorije.