U ovoj laboratorijskoj vježbi smo radili s nenadziranim strojnim učenjem, odnosno grupiranjem podataka pomoću 
Kmeans algoritma i Hijerarhijskim grupiranjem

1.) U prvom zadatku smo generirali podatke i ubacivali ih u K-means algoritam te smo analizirali rezultate.

2.) U drugom zadatku smo grafički prikazivali kriterijsku funkciju te smo analizirali dobivenu krivulju kako bi mogli očitati 
    optimalni broj klastera za podatke u tom zadatku.
    
3.) U trećem zadatku smo prikazivali podake iz prvog zadatka pomoću dendograma.

4.) U četvrtom zadatku smo K-means algoritam primjenili na piksele grayscale slike kako bi saželi sliku.

5.) Peti zadatak je sličan četvrtom, samo je slika u boji.

6.) U šestom zadatku implementiramo vlastiti K-means algoritam.
 