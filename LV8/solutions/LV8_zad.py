import numpy as np
import pandas as pd
import os
import shutil
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.preprocessing import image_dataset_from_directory

#structure test data
testImageData = pd.read_csv("archive/Test.csv")
testClassPath = testImageData.values[:, -2:]

try:
    os.mkdir("archive/Test_Dir")
except:
    pass

for image in testClassPath:
    classId = image[0]
    imagePath = "archive/" + image[1]
    
    newPath = "archive/Test_Dir/" + str(classId)
    
    try:
        os.mkdir("archive/Test_Dir/" + str(classId))
    except:
        pass
    shutil.copy2(imagePath, newPath)
    
    
    #load train and test data
train_ds = image_dataset_from_directory(
directory='archive/Train/',
labels='inferred',
label_mode='categorical',
batch_size=32,
image_size=(48, 48))

test_ds = image_dataset_from_directory(
directory='archive/Test_Dir/',
labels='inferred',
label_mode='categorical',
batch_size=32,
image_size=(48, 48))

#build model
model = keras.Sequential()
model.add(layers.Conv2D(32,(3,3), activation='relu', input_shape=(48, 48, 3)))
model.add(layers.Conv2D(32,(3,3), activation='relu'))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Dropout(0.2))
model.add(layers.Conv2D(64,(3,3), activation='relu'))
model.add(layers.Conv2D(64,(3,3), activation='relu'))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Dropout(0.2))
model.add(layers.Conv2D(128,(3,3), activation='relu'))
model.add(layers.Conv2D(128,(3,3), activation='relu'))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Dropout(0.2))
model.add(layers.Flatten())
model.add(layers.Dense(units=512, activation='relu'))
model.add(layers.Dense(units=43, activation='softmax'))

#train model
model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

model.fit_generator(train_ds, epochs = 3)

#test model
loss_and_metrics = model.evaluate(test_ds)
print(loss_and_metrics)
