### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973–74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset

Na ovom laboratoriju smo se upoznavali s korištenjem korisnih biblioteka kao što su re, Numpy i Matplotlib

U prvom i drugom zadatku smo tražili stringove koji sadrže određene znakove pomoću re biblioteke.

U trećem zadatku smo generirali random podatke pomoću Numpy biblioteke te ih grafički prikazali pomoću Matplotlib biblioteke.

Četvrti zadatak je vrlo sličan trečem, simuliramo bacanje kockice pomoću np.random.randint() metode te ih grafički prikazujemo pomoću histograma.

U petom zadatku izvlačimo podatke iz datoteke mtcars.csv te ih grafički prikazujemo pomoću Matplotlib datoteke.

U šestom zadatku pokušavamo posvijetliti sliku manipulacijom matrice.

Dodatni zadatak je proširenje na 4. zadatak. Bacanje kockice simuliramo 30 puta te tu radnju ponavljamo 1000 puta i 10000 puta. Grafički prikazujemo srednju vrijednost i standardnu devijaciju ta dva pokusa.