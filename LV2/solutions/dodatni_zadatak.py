import numpy as np
import matplotlib.pyplot as plt 

avg = []
stand_var = []

def roll_dice():
    dice_rolls = np.random.randint(1, 7, 30)
    return dice_rolls


for i in range(1000):
    rolls = roll_dice()
    avg.append(rolls.mean())
    stand_var.append(rolls.std())
    
    
plt.hist(avg, color="blue")
plt.hist(stand_var, color="red")