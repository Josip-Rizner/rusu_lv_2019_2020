import numpy as np
import matplotlib.pyplot as plt 

heightF = []
heightM = []
genders = np.random.choice(2, 30)


for gender in genders:
    if gender == 1:
        heightM.append(np.random.normal(180,7))
    else:
        heightF.append(np.random.normal(167,7))
        

plt.hist(heightM, color="blue")
plt.hist(heightF, color="red")

heightM = np.array(heightM)
heightF = np.array(heightF)
plt.scatter(heightM.mean(), 5)
plt.scatter(heightF.mean(), 5)