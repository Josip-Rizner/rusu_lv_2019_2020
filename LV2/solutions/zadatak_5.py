import re
import matplotlib.pyplot as plt

#../resources/mtcars.csv

fname = input('Enter the file name: ')

try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

info_line = True
counter = 0
sum_mpg = 0
sum_hp = 0

for line in fhand:
    if info_line == True:
        info_line = False
    else:
        counter += 1
        car = re.split(',', line)
        plt.scatter(car[1], car[4], color="blue")
        sum_mpg += float(car[1])
        sum_hp += float(car[4])

plt.xlabel("mpg")
plt.ylabel("hp")

print(sum_mpg/counter)
print(sum_hp/counter)
    