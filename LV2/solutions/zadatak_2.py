import re

#../resources/mbox-short.txt

email_names = []

fname = input('Enter the file name: ')

try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()
    
for line in fhand:
    email = re.findall('\S+@\S+', line)
    if email != []:
        name = re.split('@', email[0])
        email_names.append(name[0])


#1

"""for line in email_names:
    name = re.findall('\S*a+\S*', line)
    if name != []:
        print(name[0])"""
        
#2
for line in email_names:
    name = re.findall('^[^a]*a[^a]*$', line)
    if name != []:
        print(name[0])

#3
"""for line in email_names:
    name = re.findall('^[^a]*$', line)
    if name != []:
        print(name[0])"""


#4
"""for line in email_names:
    name = re.findall('^.*[0-9]+.*$', line)
    print(name)
    if name != []:
        print(name[0])"""

#5        
"""for line in email_names:
    name = re.findall('^[a-z]*$', line)
    print(name)
    if name != []:
        print(name[0])"""